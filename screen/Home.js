import React,{Component} from 'react';
import {
   View,
   Text,
   StyleSheet,
   SafeAreaView,
   Image,
   ScrollView
} from 'react-native'

import TabView from '../components/TabView'
import Visa from '../components/VisaCard'

class Home extends Component{
    render(){
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'#F0F1F4'}}> 
                <View>
                    <View style={{height:70}}>
                        <View style={{padding:10,marginHorizontal:20,width:200,height:20}}>
                            <Text style={{color:'#A6AAB4'}}>Your balance</Text>
                            <View style={{flexDirection:'row'}}> 
                                <Text style={{fontWeight:'bold',fontSize:32,color:'#171D33',position:'absolute'}}>$926.21</Text>
                                <Image source={require('../assets/profile_photo.png')}
                                style={{width:34,height:34,position:'absolute',left:320,top:5}}></Image>
                            </View>
                        </View>
                    </View>
                </View>
                <ScrollView scrollEventThrottle={16}> 
                    <View>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <Visa background='orange'></Visa>
                                <Visa background='#FA2E69'></Visa>
                                <Visa background='#613EEA'></Visa>
                            </ScrollView>
                        </View>
                        <TabView></TabView>
                        
                </ScrollView>
            </SafeAreaView>
        )
    }
}

export default Home