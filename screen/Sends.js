import React,{Component} from 'react';
import {
   View,
   Text,
   SafeAreaView,
   Image,
   ScrollView
} from 'react-native'

import TabView from '../components/SendTab'

class Sends extends Component{
    render(){
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'#F0F1F4'}}> 
                <View>
                    <View style={{height:70}}>
                        <View style={{padding:10,marginHorizontal:10,width:200,height:20}}>
                            <Text style={{fontFamily:'Sarabun',color:'black',fontSize:28,fontWeight:'500'}}>Send money</Text>
                            <Image source={require('../assets/profile_photo.png')}
                                style={{width:34,height:34,position:'absolute',left:320,top:10}}></Image>
                        </View>
                    </View>
                </View>
                
                <TabView></TabView>
                
            </SafeAreaView>
        )
    }
}

export default Sends