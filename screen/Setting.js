import React,{Component} from 'react';
import {
   View,
   Text,
   SafeAreaView,
   Image,
   ScrollView
} from 'react-native'

import SetIcon from '../components/SetIcon'

class Sends extends Component{
    render(){
        return(
            <SafeAreaView style={{flex:1,backgroundColor:'#F0F1F4'}}> 
                <View>
                    <View style={{height:70}}>
                        <View style={{padding:10,marginHorizontal:10,width:200,height:20}}>
                            <Text style={{fontFamily:'Sarabun',color:'black',fontSize:28,fontWeight:'500'}}>Setting</Text>
                            <Image source={require('../assets/profile_photo.png')}
                                style={{width:34,height:34,position:'absolute',left:320,top:10}}></Image>
                        </View>
                    </View>
                </View>

               <View style={{marginLeft:10}}>
                    <View flexDirection='row'>
                        <SetIcon background='white' icon='warning' iconname='Information' color='purple' type='settings'></SetIcon>
                        <SetIcon background='white' icon='lock' iconname='Password' color='purple' type='settings'></SetIcon>
                    </View>
                    <View flexDirection='row'>
                        <SetIcon background='white' icon='mail' iconname='    Email' color='purple' type='settings'></SetIcon>
                        <SetIcon background='white' icon='call' iconname='Phone Number' color='purple' type='settings'></SetIcon>
                    </View>
                    <View flexDirection='row'>
                        <SetIcon background='white' icon='clock' iconname='Notification' color='purple' type='settings'></SetIcon>
                        <SetIcon background='white' icon='pie' iconname='Currency' color='purple' type='settings'></SetIcon>
                    </View>
                    <View flexDirection='row'>
                        <SetIcon background='white' icon='happy' iconname='language' color='purple' type='settings'></SetIcon>
                        <SetIcon background='white' icon='person' iconname='Account' color='purple' type='settings'></SetIcon>
                    </View>
                    <View flexDirection='row'>
                        <SetIcon background='white' icon='mic-off' iconname='Privacy Policy' color='purple' type='settings'></SetIcon>
                        <SetIcon background='white' icon='bookmark' iconname='Term of Use' color='purple' type='settings'></SetIcon>
                    </View>    
               </View> 
               
               {/* <SetIcon></SetIcon> */}
                
            </SafeAreaView>
        )
    }
}

export default Sends