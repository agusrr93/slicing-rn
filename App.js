// import React, { Component } from "react";
// import {
//   Text,
//   View,
//   Image
// } from "react-native";

// import Icon from 'native-base'
// import {createBottomTabNavigator} from 'react-navigation'

// import Home from './screen/Home'
// import Saved from './screen/Save'
// import Sends from './screen/Sends'
// import Inbox from './screen/Inbox'
// import Setting from './screen/Setting'


// export default createBottomTabNavigator({
//     Home:{
//         screen:Home,
//         navigationOption:{
//           tabBarLabel:'HOME',
//           tabBarIcon:({tintColor})=>{
//             return <Icon name="home" color={tintColor} />
//             // return <Image source={require('./assets/home.png')} style={{height:24,width:24,tintColor:tintColor}}></Image>
//           }    
//         }
//     },
//     Saved:{
//         screen:Saved,
//         navigationOption:{
//           tabBarLabel:'WALLETS',
//           tabBarIcon:({tintColor})=>{
//             return <Image source={require('./assets/wallets.png')} style={{height:24,width:24,tintColor:tintColor}}></Image>
//           }    
//         }
//     },
//     Sends:{
//         screen:Sends,
//         navigationOption:{
//           tabBarLabel:'',
//           tabBarIcon:({tintColor})=>{
//             return <Image source={require('./assets/send_circle.png')} style={{height:24,width:24,tintColor:tintColor}}></Image>
//           }    
//         }
//     },
//     Inbox:{
//         screen:Inbox,
//         navigationOption:{
//           tabBarLabel:'',
//           tabBarIcon:({tintColor})=>{
//             return <Image source={require('./assets/reports.png')} style={{height:24,width:24,tintColor:tintColor}}></Image>
//           }    
//         }
//     },
//     Settings:{
//       screen:Setting,
//       navigationOption:{
//         tabBarLabel:'SETTING',
//         tabBarIcon:({tintColor})=>{
//           return <Image source={require('./assets/settings.png')} style={{height:24,width:24,tintColor:tintColor}}></Image>
//         }
//       }
//   },
// },
//   { 
//     tabBarOptions: {
//        showIcon: true,
//        showLabel: true,
//        activeTintColor: 'red',
//        labelStyle: {},
//   }}
// )
import React, { Component } from "react";
import Home from './screen/Home'
import Saved from './screen/Save'
import Sends from './screen/Sends'
import Inbox from './screen/Inbox'
import Setting from './screen/Setting'
import { TabNavigator } from "react-navigation";
import { Button, Text, Icon, Footer, FooterTab } from "native-base";
export default (MainScreenNavigator = TabNavigator(
  {
    Home: { screen: Home },
    Saved: { screen: Saved },
    Sends: { screen: Sends },
    Inbox:{screen:Inbox},
    Setting:{screen:Setting}
  },
  {
    tabBarPosition: "bottom",
    tabBarComponent: props => {
      return (
        <Footer style={{backgroundColor:'white'}}>
          <FooterTab style={{backgroundColor:'white'}}>
            <Button
              vertical
              active={props.navigationState.index === 0}
              onPress={() => props.navigation.navigate("Home")}>
              <Icon name="home" style={color='purple'}/>
              <Text>Home</Text>
            </Button>
            <Button
              vertical
              active={props.navigationState.index === 1}
              onPress={() => props.navigation.navigate("Saved")}>
              <Icon name="briefcase" color='purple'/>
              <Text>Saved</Text>
            </Button>
            <Button
              vertical
              active={props.navigationState.index === 2}
              onPress={() => props.navigation.navigate("Sends")}>
              <Icon name="business" />
              <Text>Sends</Text>
            </Button>
            <Button
              vertical
              active={props.navigationState.index === 3}
              onPress={() => props.navigation.navigate("Inbox")}>
              <Icon name="mail"/>
              <Text>Inbox</Text>
            </Button>
            <Button
              vertical
              active={props.navigationState.index === 4}
              onPress={() => props.navigation.navigate("Setting")}>
              <Icon name="settings" />
              <Text>Setting</Text>
            </Button>
          </FooterTab>
        </Footer>
      );
    }
  }
));

