import React, { Component } from 'react';
import {Text,View,Image} from 'react-native'
 
 export default class Visa extends Component {
    render() {
      return (
        <View style={{height:174,width:295,backgroundColor:this.props.background,margin:10,borderRadius:6,shadowOpacity:10}}>
                    <Image source={require('../assets/visa-pay-logo.png')} style={{left:20,top:6,width:40,height:40}}></Image>
                    <Text style={{fontWeight:'normal',left:20,top:30,fontSize:26,color:'white'}}>****   ****   ****   9653</Text>
                    <View flexDirection='row'>
                            <Text style={{position:'absolute',left:20,fontSize:12,top:40,color:'white'}}>CARD HOLDER</Text>
                            <Text style={{position:'absolute',left:220,fontSize:12,top:40,color:'white'}}>EXPIRES</Text>      
                    </View>
                    <View flexDirection='row'>
                            <Text style={{fontWeight:'bold',position:'absolute',left:20,fontSize:16,top:60,color:'white'}}>AGUS R R</Text>
                            <Text style={{fontWeight:'bold',position:'absolute',left:220,fontSize:16,top:60,color:'white'}}>02/21</Text>      
                    </View>                    
        </View>
      );
    }
  }