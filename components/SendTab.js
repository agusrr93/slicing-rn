import React, { Component } from 'react';
import {Text,View,Image,ScrollView} from 'react-native'
import { Container, Header, Content, Tab, Tabs,Button } from 'native-base';
import History from './HistoryView'
import Visa from './MiniVisaCard'
import Profile from './Profile'
import Form from './SearchInput'

export default class TabsExample extends Component {
  render() {
    return (
      <Container>
        <Tabs tabStyle={{backgroundColor:'#F0F1F4'}} locked = {true} tabBarUnderlineStyle={{borderBottomColor:'orange',borderBottomWidth:3}}>
          <Tab heading="Card" activeTextStyle={{color:'black'}} textStyle={{color: '#A6AAB4'}} tabStyle={{backgroundColor:'#F0F1F4'}} activeTabStyle={{backgroundColor: 'white'}}>
            <View>
                <Text style={{marginLeft:20,marginTop:10}}>Select credit card</Text>
                <ScrollView scrollEventThrottle={16}> 
                  <View flexDirection='row'>
                        <ScrollView horizontal={true} showsHorizontalScrollIndicator={true}>
                              <Visa background='orange'></Visa>
                              <Visa background='#FA2E69'></Visa>
                              <Visa background='#613EEA'></Visa>
                        </ScrollView>
                        <View style={{width:140, borderStyle: 'dashed',justifyContent:'center',
    borderWidth: 1,height:140,marginTop:10,backgroundColor:'white',marginLeft:20,marginRight:20}}>
                                <Image source={require('../assets/new.png')} style={{marginLeft:48,height:40,width:40}}></Image>
                                <Text style={{marginTop:10,marginLeft:50}}>New</Text>
                                <Text style={{marginLeft:35}}>Credit Card</Text>
                        </View>
                  </View>

                  <View flexDirection='row'>
                      <Text style={{marginLeft:20,marginTop:10}}>Recipient</Text>
                      <Text style={{marginLeft:259,marginTop:20,color:'purple'}}>Show all</Text>
                  </View>
                  
                  <View flexDirection='row'>
                      <Profile background='purple' icon='home' color='white'></Profile>
                      <Profile background='#F0F1F4' icon='home' color='grey'></Profile>
                  </View>

                  <Text style={{marginLeft:20,marginTop:10}}>Transaction Status</Text>
                  <Form plaintext='amount' icon='calculator'></Form>
                  <Form plaintext='Description (optional)' icon='mail'></Form>
                  <Button style={{justifyContent:'center',marginLeft:50,width:300,height:50,backgroundColor:'purple'}}><Text style={{color:'white'}}> Confirm </Text></Button>
                </ScrollView>
                    
            </View>
          </Tab>
          <Tab heading="Bank" activeTextStyle={{color:'black'}}	textStyle={{color: '#A6AAB4'}} tabBarUnderlineStyle={{color:'orange'}} tabStyle={{backgroundColor: 'white'}} activeTabStyle={{backgroundColor: 'white'}}>
              <View>
                  <Text style={{marginLeft:20,marginTop:20}}>Today</Text>
                  <History></History>
                  <History></History>
                  <Text style={{marginLeft:20,marginTop:20}}>Yesterday</Text>
                  <History></History>
                  <History></History>
                  <History></History>
              </View>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
