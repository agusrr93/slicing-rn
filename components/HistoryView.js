
import React, { Component } from 'react';
import {View,Image,Text} from 'react-native'

export default class RegularTextboxExample extends Component {
  render() {
    return (
        <View>
                <View style={{marginTop:20,marginLeft:20,marginRight:20,width:370,height:80,backgroundColor:'#F0F1F4',borderRadius:6}}>
                <View flexDirection='row'> 
                    <Image source={require('../assets/profile_photo.png')} style={{marginTop:20,marginBottom:20,marginLeft:10,marginRight:10}}>
                    </Image>
                    <Text style={{margin:20,fontSize:14,fontWeight:'500'}}> 
                        <Text style={{color:'#A6AAB4'}}>to : </Text>  
                        <Text>Franshiro Arnoldy {"\n"}</Text>                      
                        <Text style={{color:'#A6AAB4'}}>27 Februari 2019</Text>
                    </Text>
                    <Text style={{margin:20,fontSize:27,color:'#FA2E69',fontWeight:'bold'}}>
                        $972.00
                    </Text>
                    </View>
                </View>
        </View>
    );
  }
}


