import React, { Component } from 'react';
import {Text,View,Image} from 'react-native'
import {Icon} from 'native-base'
 
 export default class Profile extends Component {
    render() {
        console.log(this.props)
      return (
        <View style={{marginLeft:25,height:110,width:150,justifyContent:'center',backgroundColor:this.props.background,margin:10,borderRadius:6,shadowOpacity:10}}>
                {this.props.type=='settings'?
                    <View>
                        <Icon name={this.props.icon} style={{left:55,top:6,width:40,height:40,color:'#613EEA'}}></Icon>
                        <Text style={{marginLeft:35,marginTop:10,color:this.props.color}}>{this.props.iconname}</Text>
                    </View>       :<View><Image source={require('../assets/profile_photo.png')} style={{left:55,top:6,width:42,height:42}}></Image>
                    <Text style={{marginLeft:55,marginTop:10,color:this.props.color}}>Frans</Text>      
                        <Text style={{marginLeft:45,color:this.props.color}}>Ferdinand</Text></View>}   
        </View>
      );
     
    }
  }