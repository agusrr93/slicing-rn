import React, { Component } from 'react';
import {Text,View} from 'react-native'
import { Container, Header, Content, Tab, Tabs } from 'native-base';
import SearchView from './SearchInput'
import History from './HistoryView'

export default class TabsExample extends Component {
  render() {
    return (
      <Container>
        <Tabs tabStyle={{backgroundColor:'#F0F1F4'}} tabBarUnderlineStyle={{borderBottomColor:'orange',borderBottomWidth:3}}>
          <Tab heading="Sent" activeTextStyle={{color:'black'}} textStyle={{color: '#A6AAB4'}} tabStyle={{backgroundColor:'#F0F1F4'}} activeTabStyle={{backgroundColor: 'white'}}>
            <SearchView plaintext='Search transaction' icon='search'></SearchView>
            <View>
                <Text style={{marginLeft:20,marginTop:20}}>Today</Text>
                <History></History>
                <History></History>
                <Text style={{marginLeft:20,marginTop:20}}>Yesterday</Text>
                <History></History>
                <History></History>
                <History></History>
            </View>
          </Tab>
          <Tab heading="Received" activeTextStyle={{color:'black'}}	textStyle={{color: '#A6AAB4'}} tabBarUnderlineStyle={{color:'orange'}} tabStyle={{backgroundColor: 'white'}} activeTabStyle={{backgroundColor: 'white'}}>
            <SearchView plaintext='Search transaction' icon='search'></SearchView>
              <View>
                  <Text style={{marginLeft:20,marginTop:20}}>Today</Text>
                  <History></History>
                  <History></History>
                  <Text style={{marginLeft:20,marginTop:20}}>Yesterday</Text>
                  <History></History>
                  <History></History>
                  <History></History>
              </View>
          </Tab>
        </Tabs>
      </Container>
    );
  }
}
