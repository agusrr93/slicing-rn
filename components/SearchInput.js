import React, { Component } from 'react';
import { Container, Header, Content, Input, Item,Icon} from 'native-base';
export default class RegularTextboxExample extends Component {
  render() {
    return (
          <Item rounded style={{width:380,height:25,marginTop:10,marginBottom:20,marginLeft:20,marginRight:20,padding:10}}>
            <Input placeholder={this.props.plaintext} style={{color:'#A6AAB4',fontSize:12}}/> 
            <Icon active name={this.props.icon} style={{color:'#A6AAB4',fontSize:12}} />
          </Item>
    );
  }
}

